import HeaderOpItem from "./HeaderOpItem"
import {
    DotsVerticalIcon,
    MapIcon,
    NewspaperIcon,
    PhotographIcon,
    PlayIcon,
    SearchIcon
} from "@heroicons/react/outline";


function HeaderOptions() {
    return (
        <div className="flex w-full text-gray-700 
        justify-evenly text-sm lg:text-base
         lg:justify-start lg:space-x-36 lg:pl-52 border-b">
            
            {/* Left  */}
            <div className="flex space-x-6">
          <HeaderOpItem Icon={SearchIcon} title="All" selected/>
          <HeaderOpItem Icon={PhotographIcon} title="Images"/>
          <HeaderOpItem Icon={PlayIcon} title="Videos"/>
          <HeaderOpItem Icon={NewspaperIcon} title="News" />
          <HeaderOpItem Icon={MapIcon} title="Maps"/>
          <HeaderOpItem Icon={DotsVerticalIcon} title="More" /> 
            </div>

            {/* Right */}
            <div className="flex space-x-4">
                <p className="link">Settings</p>
                <p className="link">Tools</p>
            </div>
        </div>
    )
}

export default HeaderOptions
